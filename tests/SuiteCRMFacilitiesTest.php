<?php


namespace Libriciel\LibSuiteCRM;

use Exception;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class SuiteCRMFacilitiesTest extends TestCase {

    /**
     * @throws LibSuiteCRMException
     * @throws ReflectionException
     */
    public function testGetCustomerInfo()
    {

        $curlWrapperForSugar = $this->getMockBuilder(CurlWrapperForSugar::class)
            ->disableOriginalConstructor()
            ->getMock();

        $curlWrapperForSugar->expects($this->any())
            ->method('call')
            ->will($this->returnCallback(function ($url, $post_info) {
                if ($post_info['method'] == 'login') {
                    return file_get_contents(__DIR__ . "/fixtures/login.json");
                }
                if ($post_info['method'] == 'get_entry_list') {
                    return file_get_contents(__DIR__ . "/fixtures/get_entry_products.json");
                }
                throw new Exception("Methode {$post_info['method']} inconnue");
            }));

        /** @var CurlWrapperForSugar $curlWrapperForSugar */


        $suiteCRMConfiguration = new SuiteCRMConfiguration();

        $suiteCRMConfiguration->suite_crm_url = "URL_SUITE";
        $suiteCRMConfiguration->suite_crm_login = "LOGIN_SUITE";
        $suiteCRMConfiguration->suite_crm_password = "PASSSWORD_SUITE";

        $sugarWrapper = new SugarWrapper($suiteCRMConfiguration);
        $sugarWrapper->setCurlWrapperForSugar($curlWrapperForSugar);

        $suiteCRMFacilities = new SuiteCRMFacilities($suiteCRMConfiguration);
        $suiteCRMFacilities->setSugarWrapper($sugarWrapper);

        $result = $suiteCRMFacilities->getProducts();
        $this->assertEquals(array(
            0 =>
                array(
                    'id' => '1850046a-454d-2d05-8098-4fc77c2aa9bc',
                    'name' => 'Web-GFC',
                    'tag_c' => 'WG',
                ),
            1 =>
                array(
                    'id' => '2efd3976-747f-1d9c-cbf6-590090fadbcd',
                    'name' => 'web-CIL',
                    'tag_c' => 'CI',
                ),
            2 =>
                array(
                    'id' => '3221cea0-16a1-7a46-b78f-58d544d8c5ea',
                    'name' => 'Carnet_de_temps_15_h',
                    'tag_c' => 'C15',
                ),
            ),
            $result
        );
    }

}