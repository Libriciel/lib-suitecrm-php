<?php

namespace Libriciel\LibSuiteCRM;

class SuiteCustomerUser extends SuiteObject {

	public $userLogin;
	public $userTitle;
	public $userFirstname;
	public $userLastname;
	public $userEmail;
	public $userCustomerID;
	public $validID;
	public $userPhone;
	public $userFax;
	public $userMobile;
	public $userStreet;
	public $userZip;
	public $userCity;
	public $userCountry;
	public $userComment;
	public $userSugarID;
	public $admin_referent_portail;

	protected function getMandatoryFields(){
		return array($this->userLogin,
			$this->userFirstname,
			$this->userLastname,
			$this->userLastname,
			$this->validID
		);
	}

}