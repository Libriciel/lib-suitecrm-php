<?php

namespace Libriciel\LibSuiteCRM;


class CarnetDeTemps {

	const KEY_TOTAL = 'total';
	const KEY_CONSOMME = 'consomme';
	const KEY_RESTANT = 'restant';

	private $sugarRetrieveInfo;
	private $map_product;
	private $queuesID;

	private $temps_restant;

	/**
	 * CarnetDeTemps constructor.
	 * @param SugarRetrieveInfo $sugarRetrieveInfo
	 * @param array $map_product associative array with :
	 * 					key->SugarID of "Carnet de temps" maint_contrat_maintenance and
	 * 					value -> allowing time for this "Carnet de temps"
	 * @param array $queuesID array of OTRS Queues ID  for "Carnet de temps"
	 */
	public function __construct(
		SugarRetrieveInfo $sugarRetrieveInfo,
		array $map_product,
		array $queuesID
	) {
		$this->sugarRetrieveInfo = $sugarRetrieveInfo;
		$this->map_product = $map_product;
		$this->queuesID = $queuesID;
	}

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
	public function getAllTempsRestant(){
		if (! $this->temps_restant) {
			$this->temps_restant = $this->calculateTempsRestant();
		}
		return $this->temps_restant;
	}

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
	private function calculateTempsRestant(){
		$temps_achete = $this->sugarRetrieveInfo->retrieveCarnetDeTemsps($this->map_product);

		$result = array();

		foreach($temps_achete as $customer_id => $temps_total){
			$result[$customer_id] = array(
				self::KEY_TOTAL => $temps_total,
			);
		}
		return $result;
	}

    /**
     * @param $otrs_customer_id
     * @return bool|mixed
     * @throws LibSuiteCRMException
     */
	public function getCarnetDeTempsReste($otrs_customer_id){
		$all_temps_restant = $this->getAllTempsRestant();
		if (empty($all_temps_restant[$otrs_customer_id])){
			return false;
		}
		return $all_temps_restant[$otrs_customer_id];
	}

}