<?php

namespace Libriciel\LibSuiteCRM;

class SugarRetrieveInfo {


	const MODULE_CONTRAT_MAINTENANCE = 'AOS_Contracts';

	const CONTRAT_MAINTENANCE_ACCOUNT_RELATIONSHIP = 'accounts';
	const CONTRAT_MAINTEANCE_PRODUCT_RELATIONSHIP = 'aos_product_categories_aos_contracts_1';
	const CONTRAT_MAINTENANCE_CONTACT_RELATIONSHIP = 'aos_contracts_contacts_1';

	/**
	 * @const array
	 */
	public static $contrat_field_to_retrieve = array(
		'id',
		'name',
		'status',
		'contact',
		'contact_id',
		'hotline_c'
	);

	/**
	 * @const array
	 */
	public static $account_fields_to_retrieve = array(
		'id',
		'name',
		'officenumber_c',
		'billing_address_street',
		'billing_address_city',
		'billing_address_postalcode',
		'billing_address_country',
		'commentaire_suitecrm_c',
        'assigned_user_id',
        'assigned_user_name'
	);

	/**
	 * @const array
	 */
	public static $contact_fields_to_retrieve = array(
		'id',
		'name',
		'email1',
		'salutation',
		'first_name',
		'last_name',
		'phone_work',
		'phone_mobile',
		'phone_fax',
		'primary_address_street',
		'primary_address_city',
		'primary_address_postalcode',
		'primary_address_country',
        'admin_referent_portail_c'
	);

	/**
	 * @const array
	 */
	public static $link_name_to_fields_array_for_contrat = array(
		array('name' => self::CONTRAT_MAINTENANCE_ACCOUNT_RELATIONSHIP, 'value' => array('id','name','officenumber_c')),
		array('name'=> self::CONTRAT_MAINTEANCE_PRODUCT_RELATIONSHIP,'value'=>array('id','name')),
		array('name'=> self::CONTRAT_MAINTENANCE_CONTACT_RELATIONSHIP,'value'=>array('id','name'))
	);


	private $sugarWrapper;

	public function __construct(SugarWrapper $sugarWrapper){
		$this->sugarWrapper = $sugarWrapper;
	}

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
	public function retrieveUsersInfo(){
		$contrats = $this->retrieveContratMaintenance();
		$id_list = array();

		foreach($contrats as $contrat){
			foreach($contrat['users'] as $user) {
				$id_list[] = $user['user_id'];
			}
		}

		$users_list = $this->sugarWrapper->getEntries('Contacts',$id_list,self::$contact_fields_to_retrieve);

		foreach($contrats as $contrat){
			foreach($contrat['users'] as $user) {
				$users_list[$user['user_id']]['products'][$contrat['product_id']] = $contrat['product_name'];
				$users_list[$user['user_id']]['account'][$contrat['account_id']] = $contrat['account_name'];
			}
		}
		return $users_list;
	}

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
	public function retrieveAccountsInfo(){
		$contrats = $this->retrieveContratMaintenance();
		$id_list = array();

		foreach($contrats as $contrat){
			$id_list[] = $contrat['account_id'];
		}
		$accounts_list = $this->sugarWrapper->getEntries('Accounts',$id_list,self::$account_fields_to_retrieve);
		foreach($contrats as $contrat){
			$accounts_list[$contrat['account_id']]['products'][$contrat['product_id']] = $contrat['product_name'];
		}

		return $accounts_list;
	}

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
	public function retrieveContratMaintenance(){
		$result = array();
		$offset = 0;
		$nb_retrieve = 1000;
		$max = 0;

		do {
			$get_entry_list_result = $this->sugarWrapper->getEntryList(
				self::MODULE_CONTRAT_MAINTENANCE,
				"",
				"",
				$offset,
				self::$contrat_field_to_retrieve,
				self::$link_name_to_fields_array_for_contrat,
				$nb_retrieve,
				0,
				false
			);

			foreach($get_entry_list_result['entry_list'] as $i => $entry) {

				if ($entry['name_value_list']['status']['value'] == 'resilie') {
					continue;
				}

				$entry_result = array();

				foreach (array('id','name','status','hotline_c') as $j => $field_name) {
					$entry_result[$field_name] = $entry['name_value_list'][$field_name]['value'];
				}
				$entry_result['account_id'] = '';
				$entry_result['account_name'] = '';
				$entry_result['product_id'] = '';
				$entry_result['product_name'] = '';
				$entry_result['users'] = array();
				foreach($get_entry_list_result['relationship_list'][$i]['link_list'] as $link_list_element){

					if ($link_list_element['name'] == self::CONTRAT_MAINTEANCE_PRODUCT_RELATIONSHIP){
						$entry_result['product_id'] = $link_list_element['records'][0]['link_value']['id']['value'];
						$entry_result['product_name'] = $link_list_element['records'][0]['link_value']['name']['value'];
					}
					if ($link_list_element['name'] ==  self::CONTRAT_MAINTENANCE_ACCOUNT_RELATIONSHIP){
						$entry_result['account_id'] = $link_list_element['records'][0]['link_value']['id']['value'];
						$entry_result['account_name'] = $link_list_element['records'][0]['link_value']['officenumber_c']['value'];
					}
					if ($link_list_element['name'] == self::CONTRAT_MAINTENANCE_CONTACT_RELATIONSHIP){
						foreach($link_list_element['records'] as $records){
							$entry_result['users'][] = array('user_id'=>$records['link_value']['id']['value'],'user_name'=>$records['link_value']['name']['value']);
						}
					}
				}

				if (! empty($entry['name_value_list']['contact_id']['value'])){
					$entry_result['users'][] = array('user_id'=>$entry['name_value_list']['contact_id']['value'],'user_name'=>$entry['name_value_list']['contact']['value']);
				}

				$result[$entry['id']] = $entry_result;
			}
			$offset = $get_entry_list_result['next_offset'];

		} while(! empty($get_entry_list_result['entry_list']) && ($max<=0 || $offset < $max));
		return $result;
	}

    /**
     * @param array $map_product
     * @return array
     * @throws LibSuiteCRMException
     */
	public function retrieveCarnetDeTemsps(array $map_product){

		$contrat_maintenance = $this->retrieveContratMaintenance();

		$time_per_client = array();

		foreach($contrat_maintenance as $contrat){
			if (empty($map_product[$contrat['product_id']])){
				continue;
			}
			if (! isset($time_per_client[$contrat['account_name']])){
				$time_per_client[$contrat['account_name']] = 0;
			}
			$time_per_client[$contrat['account_name']] +=  	$map_product[$contrat['product_id']];
		}
		return $time_per_client;
	}

    /**
     * @throws LibSuiteCRMException
     */
	public function getProducts(){
        $entry_list = $this->sugarWrapper->getEntryList(
	        'AOS_Product_Categories',
            "",
            "",
            0,
            ['id','name','tag_c'],
            [],
            1000,
            0,
            false
        );
        if (empty($entry_list['entry_list'])){
            return [];
        }
        $result = [];
        foreach($entry_list['entry_list'] as $nb => $entry){
            foreach($entry['name_value_list'] as $id => $value){
                $result[$nb][$id] = $value['value'];
            }
        }
        return $result;
	}


}