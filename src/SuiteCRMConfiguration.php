<?php

namespace Libriciel\LibSuiteCRM;

class SuiteCRMConfiguration {

    /**
     * @var string
     * Exemple : https://suitecrm.qualif.libriciel.fr/service/v4_1/rest.php
     */
    public $suite_crm_url;

    /** @var string */
    public $suite_crm_login;

    /** @var string */
    public $suite_crm_password;
}