<?php


namespace Libriciel\LibSuiteCRM;

class SugarWrapper {

	/**
	 * @var CurlWrapperForSugar
	 */
	private $curlWrapperForSugar;

	private $suiteCRMConfiguration;

	private $session_id;

	public function __construct(SuiteCRMConfiguration $suiteCRMConfiguration){
	    $this->suiteCRMConfiguration = $suiteCRMConfiguration;
		$this->setCurlWrapperForSugar(new CurlWrapperForSugar());
	}

    /**
     * @param CurlWrapperForSugar $curlWrapperForSugar
     */
	public function setCurlWrapperForSugar(CurlWrapperForSugar $curlWrapperForSugar){
		$this->curlWrapperForSugar = $curlWrapperForSugar;
	}

    /**
     * @param $module_name
     * @param $query
     * @param $order_by
     * @param $offset
     * @param array $select_fields
     * @param array $link_name_to_fields_array
     * @param $max_results
     * @param $deleted
     * @param $favorites
     * @return mixed
     * @throws LibSuiteCRMException
     */
	public function getEntryList($module_name,$query,$order_by,$offset,array $select_fields,array $link_name_to_fields_array,$max_results,$deleted,$favorites){
		$get_entry_list_parameters = array(
			'session' => 0,
			'module_name' => $module_name,
			'query' => $query,
			'order_by' => $order_by,
			'offset' =>  $offset,
			'select_fields' => $select_fields,
			'link_name_to_fields_array' => $link_name_to_fields_array,
			'max_results' => $max_results,
			'deleted' => $deleted,
			'Favorites' => $favorites,
		);
		return $this->call('get_entry_list',$get_entry_list_parameters);
	}

    /**
     * @param $module_name
     * @return mixed
     * @throws LibSuiteCRMException
     */
	public function getModuleFields($module_name){
		$param = array('session'=>0,'module_name'=>$module_name,array());
		return $this->call('get_module_fields',$param);
	}

    /**
     * @param $module_name
     * @param $module_id
     * @param $link_field_name
     * @param $related_module_query
     * @param $related_fields
     * @param array $related_module_link_name_to_fields_array
     * @param $deleted
     * @param string $order_by
     * @param int $offset
     * @param bool $limit
     * @return mixed
     * @throws LibSuiteCRMException
     */
	public function getRelationships($module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array = array(),$deleted=false,$order_by='',$offset=0,$limit=false){
		$parameters = array(
			'session' => 0,
			'module_name' => $module_name,
			'module_id' => $module_id,
			'link_field_name' => $link_field_name,
			'related_module_query' => $related_module_query,
			'related_fields' => $related_fields,
			'related_module_link_name_to_fields_array' => $related_module_link_name_to_fields_array,
			'deleted' => $deleted,
			'order_by' =>$order_by,
			'offset' => $offset,
			'limit' => $limit,
		);
		return $this->call('get_relationships',$parameters);
	}

    /**
     * @param $module_name
     * @param $id
     * @param $select_fields
     * @param $link_name_to_fields_array
     * @return mixed
     * @throws LibSuiteCRMException
     */
	public function getEntry($module_name,$id,$select_fields,$link_name_to_fields_array ){
		$parameters = array(
			'session' => 0,
			'module_name' => $module_name,
			'id' => $id,
			'select_fields' => $select_fields,
			'link_name_to_fields_array' => $link_name_to_fields_array,
		);
		return $this->call('get_entry',$parameters);
	}

    /**
     * @param $module
     * @param $id_list
     * @param $retrieve_field_list
     * @return array
     * @throws LibSuiteCRMException
     */
	public function getEntries($module,$id_list,$retrieve_field_list){
		$result_list = array();
		$entries_list = $this->getEntriesIntern($module,$id_list,$retrieve_field_list,array());

		foreach($entries_list['entry_list'] as $i => $entry) {
			$entry_result = array();
			foreach ($retrieve_field_list as $j => $field_name) {
				$entry_result[$field_name] = '';
				if (! empty($entry['name_value_list'][$field_name]['value'])) {
					$entry_result[$field_name] = $entry['name_value_list'][$field_name]['value'];
				}
			}
			$result_list[$entry['id']] = $entry_result;
		}
		return $result_list;
	}

    /**
     * @param $module_name
     * @param array $ids
     * @param array $select_fields
     * @param $link_name_to_fields_array
     * @return mixed
     * @throws LibSuiteCRMException
     */
	private function getEntriesIntern($module_name,array $ids,array $select_fields,$link_name_to_fields_array){
		$parameters = array(
			'session' => 0,
			'module_name' => $module_name,
			'ids' => $ids,
			'select_fields' => $select_fields,
			'link_name_to_fields_array' => $link_name_to_fields_array,
		);
		return $this->call('get_entries',$parameters);
	}


    /**
     * @param $method
     * @param $param
     * @return mixed
     * @throws LibSuiteCRMException
     */
	private function call($method,$param){
		if (! $this->session_id) {
			$this->login();
		}
		$param['session'] = $this->session_id;
		return $this->post($method,$param);
	}

    /**
     * @throws LibSuiteCRMException
     */
	private function login(){
		$login_parameters = array(
			"user_auth" => array(
				"user_name" => $this->suiteCRMConfiguration->suite_crm_login,
				"password" => $this->suiteCRMConfiguration->suite_crm_password,
				"version" => "1"
			),
			"application_name" => "RestTest",
			"name_value_list" => array(),
		);

		$login_result = $this->post("login",$login_parameters);

		if (empty($login_result['id'])) {
			throw new LibSuiteCRMException("Authentication failed ({$this->suiteCRMConfiguration->suite_crm_login}) to SugarCRM ({$this->suiteCRMConfiguration->suite_crm_url}): " .
				$login_result['description']);
		}

		$this->session_id = $login_result['id'];
	}

    /**
     * @param $method
     * @param $parameters
     * @return mixed
     * @throws LibSuiteCRMException
     */
	private function post($method,$parameters){
		$post_data = array(
			"method" => $method,
			"input_type" => "JSON",
			"response_type" => "JSON",
			"rest_data" => json_encode($parameters)
		);
		$output = $this->curlWrapperForSugar->call($this->suiteCRMConfiguration->suite_crm_url,$post_data);
		if(empty($output)) {
			throw new LibSuiteCRMException("SugarCRM ({$this->suiteCRMConfiguration->suite_crm_url}) send unexpected values");
		}
		$response = json_decode($output,true);
		$this->nettoyage($response);
		return $response;
	}

	private function nettoyage(&$tab){
		foreach($tab as $i => $sub_tab){
			if (is_array($sub_tab)){
				$sub_tab = $this->nettoyage($sub_tab);
			} else {
				$sub_tab = html_entity_decode($sub_tab,ENT_QUOTES);
			}
			$tab[$i] = $sub_tab;
		}
		return $tab;
	}




}