<?php

namespace Libriciel\LibSuiteCRM;

class SugarExport {

	/**
	 * @var SugarRetrieveInfo
	 */
	private $sugarRetrieveInfo;

	public function __construct(SugarRetrieveInfo $sugarRetrieveInfo){
		$this->sugarRetrieveInfo = $sugarRetrieveInfo;
	}

    /**
     * @param $file
     * @throws LibSuiteCRMException
     */
	public function exportContratMaintenance($file){
		$contrat_list = $this->sugarRetrieveInfo->retrieveContratMaintenance();
		$file_handle = fopen($file,"w");
		$header = array("CONTRAT","STATUS CONTRAT","COMPTE","PRODUIT");
		fputcsv($file_handle,$header);

		foreach($contrat_list as $contrat){
			$line = array($contrat['name'],$contrat['status'],$contrat['account_name'],$contrat['product_name']);
			fputcsv($file_handle,$line);
		}
		fclose($file_handle);
	}

    /**
     * @param $file
     * @throws LibSuiteCRMException
     */
	public function exportCompte($file){
		$compte_list = $this->sugarRetrieveInfo->retrieveAccountsInfo();
		$file_handle = fopen($file,"w");
		$header = array("IDENTIFIANT AP","COMPTE","ADRESSE","CP","VILLE","PAYS");
		for($i=1;$i<8;$i++){
			$header[] = "PRODUIT $i";
		}
		fputcsv($file_handle,$header);

		foreach($compte_list as $compte){
			$line = array($compte['officenumber_c'],$compte['name'],$compte['billing_address_street'],
				$compte['billing_address_postalcode'],$compte['billing_address_city'],
				$compte['billing_address_country']);
			foreach($compte['products'] as $product_name){
				$line[] = $product_name;
			}
			fputcsv($file_handle,$line);
		}
		fclose($file_handle);
	}

    /**
     * @param $file
     * @throws LibSuiteCRMException
     */
	public function exportUsers($file){
		$users_list = $this->sugarRetrieveInfo->retrieveUsersInfo();
		$file_handle = fopen($file,"w");
		$header = array("NOM","EMAIL","SALUTATION","PRENOM","NOM","TELEPHONE","MOBILE","FAX","ADRESSE","VILLE","CP","PAYS","COMPTE");
		for($i=1;$i<8;$i++){
			$header[] = "PRODUIT $i";
		}
		fputcsv($file_handle,$header);
		foreach($users_list as $user){
			$line = array($user['name'],$user['email1'],$user['salutation'],$user['first_name'],$user['last_name'],
				$user['phone_work'],$user['phone_mobile'],$user['phone_fax'],$user['primary_address_street'],
				$user['primary_address_city'],$user['primary_address_postalcode'],$user['primary_address_postalcode'],
                $user['admin_referent_portail']
				);
			foreach($user['account'] as $account_name){
				$line[] = $account_name;
			}
			foreach($user['products'] as $product_name){
				$line[] = $product_name;
			}
			fputcsv($file_handle,$line);
		}
		fclose($file_handle);
	}


}