<?php

namespace Libriciel\LibSuiteCRM;

class SuiteCRMFacilities {

    /** @var SugarWrapper */
    private $sugarWrapper;

    private $suiteCRMConfiguration;

    public function __construct(SuiteCRMConfiguration $suiteCRMConfiguration) {
        $this->suiteCRMConfiguration = $suiteCRMConfiguration;
        $this->setSugarWrapper(new SugarWrapper($suiteCRMConfiguration));
    }

    public function setSugarWrapper(SugarWrapper $sugarWrapper){
        $this->sugarWrapper = $sugarWrapper;
    }

    /**
     * @param $code_ls
     * @return array|bool
     * @throws LibSuiteCRMException
     */
    public function getCustomerInfo($code_ls){
        $entry_list = $this->sugarWrapper->getEntryList(
            'Accounts',
            "accounts_cstm.officenumber_c='$code_ls'",
            "",
            0,
            SugarRetrieveInfo::$account_fields_to_retrieve,
            [],
            1,
            0,
            false
        );

        if (empty($entry_list['entry_list'][0])){
            return false;
        }
        $result = [];
        $entry = $entry_list['entry_list'][0];
        foreach($entry['name_value_list'] as $id => $value){
            $result[$id] = $value['value'];
        }

        $result['meetings'] = $this->getMeetings($result['id']);
        $result['carnet_de_temps_heures_achetees'] = $this->getCarnetDeTemps($result['id']);

        $result['contact_commercial'] = $this->getUserDetail($result['assigned_user_id']);

        return $result;
    }

    /**
     * @param $customer_id
     * @return array
     * @throws LibSuiteCRMException
     */
    public function getMeetings($customer_id){
        $entry_list = $this->sugarWrapper->getEntryList(
            'Meetings',
            "meetings.parent_id='$customer_id' AND meetings.status='To_be_planned'",
            "",
            0,
            ['id','name','aos_products_id_c','status','produit_c','libriciel_c','aos_product_categories_id_c'],
            []
            ,
            1000,
            0,
            false
        );
        if (empty($entry_list['entry_list'])){
            return [];
        }
        $result = [];
        foreach($entry_list['entry_list'] as $nb => $entry){
            foreach($entry['name_value_list'] as $id => $value){
                $result[$nb][$id] = $value['value'];
            }
        }
        return $result;
    }

    /**
     * @param $account_id
     * @return array|int
     * @throws LibSuiteCRMException
     */
    public function getCarnetDeTemps($account_id){
        $entry_list = $this->sugarWrapper->getEntryList(
            SugarRetrieveInfo::MODULE_CONTRAT_MAINTENANCE,
            "aos_contracts.contract_account_id='$account_id'",
            "",
            0,
            ['id','name'],
            [],
            1000,
            0,
            false
        );
        if (empty($entry_list['entry_list'])){
            return [];
        }
        $result = [];
        foreach($entry_list['entry_list'] as $nb => $entry){
            foreach($entry['name_value_list'] as $id => $value){
                $result[$nb][$id] = $value['value'];
            }
        }

        $total = 0;
        foreach ($result as $value){
            if (preg_match("#CT([\d+])\/#",$value['name'],$matches)){
                $total += intval($matches[1]);
            }
        }
        return $total;
    }

    /**
     * @param $user_id
     * @return array
     * @throws LibSuiteCRMException
     */
    public function getUserDetail($user_id){
        if (! $user_id){
            return [];
        }
        $entry_list = $this->sugarWrapper->getEntry(
            'Users',
            $user_id,
            ['id','name','phone_mobile','email1'],
            []
        );
        $result = [];
        foreach($entry_list['entry_list'] as $nb => $entry){
            foreach($entry['name_value_list'] as $id => $value){
                $result[$id] = $value['value'];
            }
        }
        return $result;
    }

    /**
     * @return array
     * @throws LibSuiteCRMException
     */
    public function getProducts(){
        $sugarRetrieveInfo = new SugarRetrieveInfo($this->sugarWrapper);
        return $sugarRetrieveInfo->getProducts();
    }

}