<?php

namespace Libriciel\LibSuiteCRM;

class CurlWrapperForSugar {

    /**
     * @param $url
     * @param array $post_data
     * @return bool|string
     * @throws LibSuiteCRMException
     */
	public function call($url, array $post_data) {
		$curl_properties = array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_FOLLOWLOCATION => 0,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_0,
			//CURLOPT_HEADER => 1,
			CURLOPT_POST => 1,
			CURLOPT_URL => $url,
			CURLOPT_POSTFIELDS=>$post_data,
			CURLOPT_SSLVERSION=>CURL_SSLVERSION_TLSv1,
		);

		$curlHandle = curl_init();

		foreach($curl_properties as $property => $value) {
			curl_setopt($curlHandle,$property,$value);
		}
		$output = curl_exec($curlHandle);
		$error = curl_error($curlHandle);
		if ($error){
			throw new LibSuiteCRMException("Failed to connect to SugarCRM ($url) : " . $error);
		}
		$http_code = curl_getinfo($curlHandle,CURLINFO_HTTP_CODE) ;
		if($http_code != 200){
			throw new LibSuiteCRMException("SugarCRM return error HTTP code $http_code");
		}

		return $output;
	}


}