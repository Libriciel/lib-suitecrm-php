<?php

namespace Libriciel\LibSuiteCRM;

use ReflectionClass;
use ReflectionProperty;
use ReflectionException;

abstract class SuiteObject {

	abstract protected function getMandatoryFields();

	private $param_list;
	private $has_param_list;

    /**
     * OTRSObject constructor.
     * @param array $info_from_rpc
     * @throws ReflectionException
     */
	public function __construct(array $info_from_rpc = array()){
		if (! $info_from_rpc){
			return;
		}
		foreach($this->getParamList() as $param ){
			if (isset($info_from_rpc[ucfirst($param)])) {
				$this->$param = $info_from_rpc[ucfirst($param)];
			}
		}
	}

    /**
     * @return array
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     */
	public function getArray(){
		$this->validate();
		$result = array();
		foreach($this->getParamList() as $param){
			$result[ucfirst($param)] = $this->$param;
		}
		return $result;
	}

    /**
     * @return array
     * @throws ReflectionException
     */
	private function getParamList(){
		if ($this->has_param_list){
			return $this->param_list;
		}
		$ref = new ReflectionClass($this);
		$all = $ref->getProperties(ReflectionProperty::IS_PUBLIC);
		$this->param_list = array();
		foreach($all as $properties){
			$this->param_list[] = $properties->name;
		}
		return $this->param_list;
	}

    /**
     * @throws MandatoryFieldsException
     */
	public function validate(){
		foreach($this->getMandatoryFields() as $fields){
			if (empty($fields)){
				throw new MandatoryFieldsException("Un des champs obligatoire n'est pas renseigné");
			}
		}
	}

    /**
     * @param SuiteObject $otrsCustomerCompany
     * @return bool
     * @throws ReflectionException
     */
	public function isEquals(SuiteObject $otrsCustomerCompany){
		foreach($this->getParamList() as $param){
			if ($this->$param != $otrsCustomerCompany->$param){
				return false;
			}
		}
		return true;
	}
}

