<?php

require_once(__DIR__ . "/init.php");

use Libriciel\LibSuiteCRM\SugarRetrieveInfo;
use Libriciel\LibSuiteCRM\SugarWrapper;

$method = array(
	'Users'=>'retrieveUsersInfo',
	'Accounts'=>'retrieveAccountsInfo',
	'Contracts'=>'retrieveContratMaintenance'
);


if ($argc > 1){
	$type = $argv[1];
} else {
	$type = 'help';
}

if (! in_array($type,array_keys($method))){
	echo "Usage : {$argv[0]} ".implode("|",array_keys($method))."\n";
	exit;
}



$sugarRetrieveInfo = new SugarRetrieveInfo(
    new SugarWrapper(
        SUGAR_API_URL,
        SUGAR_USERNAME,
        SUGAR_PASSWORD)
);

$method_name = $method[$type];

$info = $sugarRetrieveInfo->$method_name();

print_r($info);

