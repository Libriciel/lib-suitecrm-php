<?php

require_once(__DIR__ . "/init.php");

use Libriciel\LibSuiteCRM\SuiteCRMConfiguration;
use Libriciel\LibSuiteCRM\SuiteCRMFacilities;

$suiteCRMConfiguration = new SuiteCRMConfiguration();

$suiteCRMConfiguration->suite_crm_url = SUGAR_API_URL;
$suiteCRMConfiguration->suite_crm_login = SUGAR_USERNAME;
$suiteCRMConfiguration->suite_crm_password = SUGAR_PASSWORD;

$suiteCRMFacilities = new SuiteCRMFacilities($suiteCRMConfiguration);

try {
    $result = $suiteCRMFacilities->getProducts();
} catch (Exception $e){
    echo "Erreur : " . $e->getMessage();
    exit (-1);
}

print_r($result);
exit (0);


