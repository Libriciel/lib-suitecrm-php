
# Notes

Le repo est copié sur le gitlab de l'[Adullact](https://gitlab.adullact.net/) et disponible en public.

# Exemple d'intégration dans composer

```yaml
{

    "repositories": [
        {
            "type":"package",
            "package": {
                "name": "libriciel/lib-suitecrm-php",
                "version":"master",
                "source": {
                    "url": "https://gitlab.adullact.net/Libriciel/lib-suitecrm-php.git",
                    "type": "git",
                    "reference":"master"
                }
            }
        }
    ],

    "require": {
        "libriciel/lib-suitecrm-php": "master"

    },
    "minimum-stability": "dev"
}
```

# Exemples d'utilisation

Voir le répertoire exemple

# Docker compose

Pour se mettre dans un environnement opérationel :

```bash
docker-compose run lib bash
```
